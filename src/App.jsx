import React from 'react';

export default class AppComponent extends React.Component{
    constructor(){
        super();

        this.state = {
            pNumber: 0,
            name: "",
            lastName: ""
        }
        this.pNumberHandler = this.pNumberHandler.bind(this);
    }

    pNumberHandler(action){
        let {pNumber} = this.state;
        this.setState({
            pNumber: action === "+" ? ++pNumber : --pNumber
        });
    }

    render(){

        return (
            <div>
                This is a number: 
                <button onClick={e=>this.pNumberHandler("-")}>-</button>
                {this.state.pNumber}
                <button onClick={e=>this.pNumberHandler("+")}>+</button>
            </div>
        )
        
    }
}